import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom'; 
import Header from '../../components/Header';

const Landing = () => {
  const [userName, setUserName] = useState('');
  const navigate = useNavigate();

  const handleMulaiGameClick = () => {    
    if (userName.trim() !== '') { // Validasi input tidak boleh kosong atau hanya whitespace
      navigate(`/game/${userName}`);
    } else {
      alert('Username tidak boleh kosong'); // Tampilkan pesan kesalahan jika input kosong
    }
  };

  const handleInput = (e) => {
    setUserName(e.target.value);
  }
  
  return (
    <div className='container'>
      <Header />
      <div className='row justify-content-center mt-5 vh-100'>
        <div className='col-md-4 '>
          <div className='card p-3 d-flex flex-column align-items-center'>
            <input
              type='text'
              value={userName}
              onChange={handleInput}
              placeholder='Masukkan username'
              className='form-control'
            />
            <div className='mt-2'>
              <button className='btn btn-primary' onClick={handleMulaiGameClick}>Mulai Bermain</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Landing;
