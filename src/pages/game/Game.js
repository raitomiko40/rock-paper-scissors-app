import React, { Component } from 'react';
import '../../App.css';
import Header from '../../components/Header';
import Player from '../../components/Player';
import Bot from '../../components/Bot';
import VSResult from '../../components/VSResult';
import RefreshButton from '../../components/RefreshButton';


class Game extends Component {
  
  constructor() {
    super();
    this.state = {
      playerChoice: null,
      botChoice: null,
      result: null,
      gameInProgress: false,
    };
  }

  handlePlayerChoice = (choice) => {
    if (this.state.gameInProgress) {
      return;
    }

    const botChoices = ['kertas', 'batu', 'gunting'];
    const botChoice = botChoices[Math.floor(Math.random() * botChoices.length)];

    this.setState({ playerChoice: choice, botChoice, gameInProgress: true }, () => {
      this.calculateResult();
    });
  };

  calculateResult = () => {
    const { playerChoice, botChoice } = this.state;
    let result = '';
    if (
      (playerChoice === 'kertas' && botChoice === 'batu') ||
      (playerChoice === 'batu' && botChoice === 'gunting') ||
      (playerChoice === 'gunting' && botChoice === 'kertas')
    ) {
      result = 'Player Win';
    } else if (playerChoice === botChoice) {
      result = 'Draw';
    } else {
      result = 'Bot Win';
    }
    console.log(result);

    this.setState({ result });
  };

  resetGame = () => {
    this.setState({
      playerChoice: null,
      botChoice: null,
      result: null,
      gameInProgress: false,
    });
  };
 

  render() {
    const { playerChoice, botChoice, result } = this.state;
    

    return (
      <div className="container">
        <Header />
        <section className="container">
          <div className="row text-center">
            <Player
              playerChoice={playerChoice}
              handlePlayerChoice={this.handlePlayerChoice}
            />
            <VSResult result={result} />
            <Bot botChoice={botChoice} />
            {result && (
              <RefreshButton resetGame={this.resetGame} />
            )}
          </div>
        </section>
      </div>
    );
  }
}

export default Game;
    