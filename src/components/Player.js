import React from 'react';
import batuImg from '../assets/batu1.png'
import guntingImg from '../assets/gunting1.png'
import kertasImg from '../assets/kertas1.png'
import { useParams } from 'react-router-dom';

function Player(props) {
  const { userName } = useParams();
  return (
    <div className="col" id="Player">
      <div className="mt-5 mb-5">
        <h1>{userName}</h1>
      </div>
      <div className="mb-3 option">
        <img
          id="batu"
          className={`player ${props.playerChoice === 'batu' ? 'activePick' : ''}`}
          role="button"
          src={batuImg}
          alt="batu"
          onClick={() => props.handlePlayerChoice('batu')}
        />
      </div>
      <div className="mb-3 option">
        <img
          id="gunting"
          className={`player ${props.playerChoice === 'gunting' ? 'activePick' : ''}`}
          role="button"
          src={guntingImg}
          alt="gunting"
          onClick={() => props.handlePlayerChoice('gunting')}
        />
      </div>
      <div className="mb-3 option">
        <img
          id="kertas"
          className={`player ${props.playerChoice === 'kertas' ? 'activePick' : ''}`}
          role="button"
          src={kertasImg}
          alt="kertas"
          onClick={() => props.handlePlayerChoice('kertas')}
        />
      </div>
    </div>
  );
}

export default Player;
