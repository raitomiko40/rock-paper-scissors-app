import React from 'react';
import Logo from '../assets/logo 2.png'

function Header() {
  return (
    <header className="d-flex  align-items-center">
      <div role="button" className="back ms-3">
        <a href="index.html"><img className="" src="assets/_.png" alt="" /></a>
      </div>
      <div role="button" className="logo ms-3">
        <img src={Logo} alt="" />
      </div>
      <div className="game-title ms-3 title"><h1>ROCK PAPER SCISSORS</h1></div>
    </header>
  );
}

export default Header;
