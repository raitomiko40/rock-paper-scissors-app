import React from 'react';
import { useParams } from 'react-router-dom';

function VSResult(props) {
  const { userName } = useParams();
  const { result } = props;

  return (
    <div className="col d-flex justify-content-center align-items-center">
      {result ? (
        <div className={`d-flex justify-content-center align-items-center results ${result && result.toLowerCase()}`}>
          {result === 'Player Win' && (
            <div>{userName}<br />MENANG</div>
          )}
          {result === 'Bot Win' && (
            <div>BOT<br />MENANG</div>
          )}
          {result === 'Draw' && (
            <div>SERI</div>
          )}
        </div>
      ) : (
        <div id="vs" className="vs">VS</div>
      )}
    </div>
  );
}

export default VSResult;
