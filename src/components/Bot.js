import React from 'react';
import batuImg from '../assets/batu1.png'
import guntingImg from '../assets/gunting1.png'
import kertasImg from '../assets/kertas1.png'

function Bot(props) {
  return (
    <div className="col">
      <div className="mt-5 mb-5"><h1>Bot</h1></div>
      <div className="mb-3 option">
        <img
          id="botbatu"
          className={`bot ${props.botChoice === 'batu' ? 'activePick' : ''}`}
          src={batuImg}
          alt="batu"
        />
      </div>
      <div className="mb-3 option">
        <img
          id="botgunting"
          className={`bot ${props.botChoice === 'gunting' ? 'activePick' : ''}`}
          src={guntingImg}
          alt="gunting"
        />
      </div>
      <div className="mb-3 option">
        <img
          id="botkertas"
          className={`bot ${props.botChoice === 'kertas' ? 'activePick' : ''}`}
          src={kertasImg}
          alt="kertas"
        />
      </div>
    </div>
  );
}

export default Bot;
